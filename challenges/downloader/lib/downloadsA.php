<?php
$file = $_GET['file'];
if(preg_match("/index/i", $file) || preg_match("/descargas/i", $file) || preg_match("/lib/i", $file) || preg_match("/php/i", $file) || preg_match("/ctf/i", $file) || preg_match("//i", $file)){
    echo "<!DOCTYPE html>
        <html lang=en>
        <head>
        <title>Descargas</title>
        <link rel=icon href='../../img/favicon.ico'>
        <link href='../../../css/alertLevel4.css' rel='stylesheet'>
        </head>
        </body>
        <div class='history advertencia'>
        <br>
        <h3>El Administrador no tiene porque ver esto, dejaselo a los ingenieros.</h3>
        </div>
        <br>
        <button type=button onclick=location.href='../index.php';>Regresar</button>
        </body>
        </html>";
}
else{
    $link = '../2d3415da2929c553c74a7f7eae3a47a9/'.$file;
    if (file_exists($link)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($link));
        readfile($link);
        exit;
    }
    else{
        echo "<!DOCTYPE html>
            <html lang=en>
            <head>
            <title>Descargas</title>
            <link rel=icon href='../../img/favicon.ico'>
            <link href='../../../css/alertLevel4.css' rel='stylesheet'>
            </head>
            </body>
            <div class='history advertencia'>
            <br>
            <h3>No existe el archivo</h3>
            </div>
            <br>
            <button type=button onclick=location.href='../index.php';>Regresar</button>
            </body>
            </html>";
    }
}
?>
