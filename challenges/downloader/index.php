<?php
setcookie('user4', 'UserN', time() + 365 * 24 * 60 * 60);
setcookie('access4', 'user', time() + 365 * 24 * 60 * 60);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../img/favicon.ico">
    <title>Downloader Free4All</title>
    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <!-- ZFhOMVlYSnBieUE5UFNCQlpHMXBiaUFtSmlCaFkyTmxjMjhnUFQwZ2NtOXZkQT09 -->
    <!-- UVdSdGFXNDZJR2hsYkhBdWRIaDBJQ0JWYzJWeU9pQmhlWFZrWVM1MGVIUT0= -->
    <link href="../../css/jumbotron-narrowLevel4.css" rel="stylesheet">
    <link href="../../css/sboxLevel4.css" rel="stylesheet" type="text/css">
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</head>
<body>
    <div class="container">
      <div class="header">
        <h3 class="text-muted">Downloader Free4All</h3>
      </div>
      <div class="jumbotron">
        <h2>Downloader Free4All</h2>
        <p class="lead">Escribe el nombre del archivo que buscas y si lo tenemos se descargará a tu computadora. No olvides poner también su extensión.</p>
                <form id="tfnewsearch" method="get" action="lib/downloads.php" autocomplete="off">
                        <input type="text" class="tftextinput" placeholder="Nombre del archivo" name="file" size="50" maxlength="120"><input type="submit" value="Buscar" class="tfbutton">
                </form>
                <!-- Aqui hay un secretito :P -->
    <?php
    if(isset($_COOKIE['user4']) && isset($_COOKIE['access4'])){
        if($_COOKIE['user4'] == "Admin" && $_COOKIE['access4'] == "root"){
            echo "<form id=tfnewsearch method=get action=lib/downloadsA.php autocomplete=off> <input type=text class=tftextinput placeholder='Buscador del Administrador' name=file size=50 maxlength=120><input type=submit value=Buscar class=tfbutton> </form>";
            setcookie('user4', 'Admin', time() + 365 * 24 * 60 * 60);
            setcookie('access4', 'root', time() + 365 * 24 * 60 * 60);
        }
    }
    ?>
        <div class="tfclear"></div>
        <button type="button" class="btn btn-lg btn-info" onclick="location.href='../index.html';">Salir</button>
        </div>
      <div class="row marketing">
        <div class="col-lg-10">
          <h4>Rápido</h4>
          <p>Ofrecemos los menores tiempos de descarga.</p>
          <h4>Seguro</h4>
          <p>Puedes pedirnos subir un archivo privado. Enviándolo a d4fa@yeahoo.com</p>
          <h4>Duradero</h4>
          <p>Los archivos siempre estarán colgados en la nube.</p>
        </div>
      </div>
      <div class="footer">
        <p>&copy; Downloader Free4All 2014</p>
      </div>
    </div> <!-- /container -->
  </body>
</html>
