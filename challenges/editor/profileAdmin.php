<?php
if($_COOKIE['user'] == 'dog') {
    if($_COOKIE['level_access'] == 'adminroot') {
        $dam='';
        $user="Administrator";
    }
}
else {
    header('location: index.html');
}
?>
<!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <title>Perfil</title>
    <link href="../css/bootstrapLevel2.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/fontello.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/ads.css">
    </head>
    <body>
    <div class="navbar">
      <div class="navbar-inner">
        <div class="container"> 
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a>
          <ul class="nav nav-collapse pull-right">
            <li><a href="profileAdmin.php" class="active"></i> Profile</a></li>
          </ul>
          <div class="nav-collapse collapse">
          </div>
        </div>
      </div>
    </div>
    <div class="container profile">
      <div class="span3"> <img src="../img/mrburns.png"> </div>
      <div class="span5">
        <h1>Montgomery Burns</h1>
        <h3> Propietario de la Central nuclear de Springfield</h3>
        <p>Esposa:Gertrude Burns (fallecida)</p>
        <p>Hijos:Larry Burns</p>
          <h1>Informe de ventas "secretas"</h1>
          <table  border=3  WIDTH=500 >
            <tr border= "45" color=#fff WIDTH=300>
              <th abbr="Imagen del producto" scope="col">Imagen</th>
              <th abbr="Datos del producto" scope="col">Datos</th>
            </tr>
            <tr>
              <td border="45">
                <img src="../img/bar.jpg" width='300' height='300' alt="Imagen del ordenador portátil" />
              </td>
              <td>
                <h4><a href="../img/bar.jpg" title="Ver más información sobre el portátil">Plutonio Radioactivo</a></h4>
                <p>Comprador:  <strong>Rusia</strong></p>
              </td>
            </tr>
            <tr>
              <td border="45">
                <img src="../img/outline-34272_640.png" width='250' height='300' alt="Imagen del ordenador portátil" />
              </td>
              <td>
                <h4><a href="../img/outline-34272_640.png" title="Armamento">Armamento</a></h4>
                <p>Comprador:  <strong>Estados Unidos</strong></p>
              </td>
            </tr>
              <tr>
              <td border="45">
                <img src="../img/tank.jpg" width='300' height='300' alt="Tanque" />
              </td>
              <td>
                <h4><a href="../img/tank.jpg" title="Tanque">Tanque Militar</a></h4>
                <p>Comprador:  <strong>Estados Unidos</strong></p>
              </td>
            </tr>
              <tr>
              <td border="45">
                <img src="../img/sucker.jpg" width='300' height='300' alt="Bobo" />
              </td>
              <td>
                <h4><a href="../img/sucker.jpg" title="Bobo">Bobo</a></h4>
                <p>Comprador:  <strong>Maggie Simpson</strong></p>
              </td>
            </tr>
          </table>
        </div>
    </div>
    <div class="row social">
    </div>
    <div class="footer">
      <div class="container">
        <p class="pull-left">Hackem</p>
        <p class="pull-right"><a href="#myModal" role="button" data-toggle="modal">  CONTACT</a></p>
      </div>
    </div>
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel"><i class="icon-mail"></i> Contact Me</h3>
      </div>
      <div class="modal-body">
        <form>
          <input type="text" placeholder="Yopur Name">
          <input type="text" placeholder="Your Email">
          <input type="text" placeholder="Website (Optional)">
          <textarea rows="3" style="width:80%"></textarea>
          <br/>
          <button type="submit" class="btn btn-large"><i class="icon-paper-plane"></i> SUBMIT</button>
        </form>
      </div>
    </div>
    <script src="../js/jquery.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $(".divClass .deleteDiv").click(function(){
            $(this).parents(".divClass").animate({ opacity: 'hide' }, "slow");
        });
    });
    </script>
    <script src="../js/bootstrap.min.js"></script>
    <script>$('#myModal').modal('hidden')</script>
</body>
</html>
