<?php
session_start();
ob_start();
if(!isset($_SESSION['userR6']) || !isset($_SESSION['accessR6'])){
    $_SESSION['userR6']='';
    $_SESSION['accessR6']='';
}
$userR6=$_SESSION['userR6'];
$accessR6=$_SESSION['accessR6'];
$account = "../TCC/index.php";

if($_SESSION['userR6'] == ''){
    setcookie("user6", "", time()-3600);
    setcookie("access6", "", time()-3600);
}
else{
    if(isset($_COOKIE['user6']) && isset($_COOKIE['access6'])){
        if($_COOKIE['user6'] == "Admin" && $_COOKIE['access6'] == "toor"){
            $_SESSION['userR6'] = "Administrador";
            $account = "../TCC/indexa.php";
        }
    }
    else{
        setcookie('user6', $userR6, time() + 365 * 24 * 60 * 60); 
        setcookie('access6', $accessR6, time() + 365 * 24 * 60 * 60); 
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../../img/favicon.ico">

<script language="JavaScript">
function checkKey(evt) {
    if (evt.ctrlKey){
        alert(":P Ab chrqrf hgvyvmne Pgey!!!!!");
        }
    }
</script>

<title>Free - Market</title>

<!-- Bootstrap core CSS -->
<link href="../../../css/bootstrapLevel6.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../../../css/justified-navLevel6.css" rel="stylesheet">
<link href="../../../css/carousel.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../../../assets/js/ie-emulation-modes-warning.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../../assets/js/ie10-viewport-bug-workaround.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif] -->
</head>

<body onKeyDown="checkKey(event)">

<script language="Javascript" type="text/javascript">
//<![CDATA[

<!-- Begin
    document.oncontextmenu = function(){return false}
    // End -->
    //]]>
    </script>

    <div class="container">
        <div class="masthead">
            <h3 class="text-muted">Free - Market</h3>		
            <ul class="nav nav-justified">
              <li class="active"><a href="index.php">Inicio</a></li>
<?php

if($userR6 != ''){
    echo "<li class=dropdown>
        <a href=# class=dropdown-toggle data-toggle=dropdown>$userR6 <span class=caret></span></a>
        <ul class=dropdown-menu role=menu>
        <li><a href=$account>Datos de la Cuenta</a></li>
        <li class=divider></li>
        <li><a href=lib/signOut.php>Salir</a></li>
        </ul>
        </li>
        <li><a href=../TCP/index.php>Productos</a></li>";
}
else{
    echo
        "<li><a href=../TCL/index.html>Miembros</a></li>
        <li><a>Productos</a></li>";
}
?>
              <li><a href="../TCF/index.htm">FAQ</a></li>
            </ul>
        </div>
    <!-- Jumbotron -->
    <div class="jumbotron">
      <h1>Bienvenido al Free - Market</h1>
      <p class="lead">Aquí encontrará todo tipo de productos.</p>
    </div>

    <!-- Example row of columns -->
    <div class="row">
      <div class="col-lg-4">
        <h2 align="center">Producto Destacado</h2>
        <div align="center"><img class="img-circle" src="../../../img/p1.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;"></div>
        <p align="center">Disco Duro de 1 Petabyte</p>
      </div>
      <div class="col-lg-4">
        <h2 align="center">Producto Destacado</h2>
        <div align="center"><img class="img-circle" src="../../../img/p2.jpg" alt="Generic placeholder image" style="width: 140px; height: 140px;"></div>
        <p align="center">Lunas Personalizadas</p> 
      </div>
      <div class="col-lg-4">
        <h2 align="center">Producto Destacado</h2>
        <div align="center"><img class="img-circle" src="../../../img/p3.png" alt="Generic placeholder image" style="width: 140px; height: 140px;"></div>
        <p align="center">Dota 3</p>
      </div>
    </div>

      <form class="form col-md-12 center-block" action="lib/giveUser.php" method="post">
        <br><br>
        <div class="form-group">
      <h3>Entrégenos su Email.</h3>
            <!-- Fv rfpevor fh rznvy, fr yr qneá ha hfhnevb cnen dhr chrqn npprqre :C --> 
        </div>
        <div class="form-group">
              <input type="email" class="form-control" placeholder="Email" name="email" disabled>
        </div>
        <div class="form-group">
              <button type="submit" class="btn btn-success btn-sm" disabled>Enviar</button>
        </div>
        <br>
      </form> 
      <div align="center">
          <button type="button" class="btn btn-lg btn-info" onclick="location.href='../../index.html';">Salir</button>
      </div>
    <!-- Site footer -->
    <div class="footer">
        <p>&copy; Free - Market 2014</p>
    </div>
  </div> <!-- /container -->


  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="../../../js/bootstrap.min.js"></script>
  <script src="../../../js/docs.min.js"></script>
</body>
</html>
