<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Usuario Asignado</title>
<meta name="generator" content="Bootply" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="icon" href="../../../img/favicon.ico">
<link href="../../../css/bootstrap.min.css" rel="stylesheet">
<!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link href="../../../css/styles.css" rel="stylesheet">
</head>
<body>
<!--login modal-->
  <div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><a href="../index.php">×</a></button>
            <font color="green"><h1 class="text-center">Usuario y Password</h1></font>
        </div>
        <div class="modal-body">
            <form class="form col-md-12 center-block">
            <br>
<?php
require_once( "../connection/queries.php");

$query = new Queries();

$id = rand(1, 5);

if($_POST["email"] != ''){  				
    if(filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)){  				
        $result = $query -> giveUser( $id );  						
        foreach( $result as $value){
            $ustemp = $value[ "Usuario" ];
            $pstemp = base64_encode($value[ "Pass" ]);
        }
        echo 
            "<div class=form-group>
            <h3 align=center>$ustemp</h3>
            </div>
            <div class=form-group>
            <h3 align=center>$pstemp</h3>
            </div>
            <div class=form-group>
            <h3 align=center>:D</h3>
            </div>";
    }
    else{
        header('Location: ../msg/msgV.html');
    }
}
else{
    header('Location: ../msg/msgL.html');
}
?>

          </form>
        </div>
        <div class="modal-footer">
          <div class="col-md-12">
            <button class="btn" data-dismiss="modal" aria-hidden="true"><a href="../index.php">Cancelar</a></button>
            </div>	
        </div>
      </div>
    </div>
  </div>
    <!-- script references -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="../../../js/bootstrap.min.js"></script>
    <script src="../../../js/scripts.js"></script>
</body>
</html>
