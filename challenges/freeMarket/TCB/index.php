<?php
session_start();
ob_start();
if($_SESSION['userR6'] == "Administrador" || $_SESSION['userR6'] == ""){
    header('Location: msg/msgX.html');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../img/favicon.ico">

<title>Mikaboshi Bank</title>

<!-- Bootstrap core CSS -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../../css/coverLevel6.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../../js/ie-emulation-modes-warning.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../js/ie10-viewport-bug-workaround.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
<script language="Javascript" type="text/javascript">
//<![CDATA[

<!-- Begin
    document.oncontextmenu = function(){return false}
    // End -->
    //]]>
    </script>

    <div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">Mikaboshi Bank</h3>
              <ul class="nav masthead-nav">
                <li class="active"><a href="../TC/index.php">Inicio</a></li>
              </ul>
            </div>
          </div>
          <form class="form-signin" role="form" action="lib/verifyCard.php" method="post">
            <h2 class="form-signin-heading">Verifique su Tarjeta</h2>
            <input type="text" class="form-control" placeholder="Número de Tarjeta" name="card">
            <br>
            <p>Ejemplo: 1111 2222 3333 4444</p>
            <div class="form.group">
              <br>
              <button class="btn btn-lg btn-danger" type="submit">Verificar</button>
              <button type="button" class="btn btn-lg btn-danger" onclick="location.href='../TC/index.php';">Salir</button>
            </div>
          </form>
          <div class="mastfoot">
            <div class="inner">
              <p>&copy; Mikaboshi Bank 2014</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/docs.min.js"></script>
</body>
</html>
