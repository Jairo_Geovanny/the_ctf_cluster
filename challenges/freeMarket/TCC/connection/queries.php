<?php
require 'connection.php';

class Queries extends Connection{

    public function __construct() {
        parent::__construct();
    }

    public function chargingUsers(){
        $result = Array();
        $this->openConnection('market');
        $rSQL = $this->getQuery("SELECT * FROM market.egp");

        if( mysqli_num_rows( $rSQL ) > 0 ){
            while ( $row = mysqli_fetch_assoc( $rSQL ) ){
                array_push( $result , $row );
            }
        }

        $this->closeConnection();

        return $result;
    }
}
?>
