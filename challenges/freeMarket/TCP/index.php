<?php
session_start();
ob_start();

if($_SESSION['userR6'] == ""){
    header('Location: msg/msgX.html');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../img/favicon.ico">

<script language="JavaScript">
function checkKey(evt) {
    if (evt.ctrlKey){
        alert(":P Ab chrqrf hgvyvmne Pgey!!!!!");
  }
}
</script>

<title>Productos</title>

<!-- Bootstrap core CSS -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="../../css/dashboardLevel6.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="../../js/ie-emulation-modes-warning.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="../../js/ie10-viewport-bug-workaround.js"></script>

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body onKeyDown="checkKey(event)">

<script language="Javascript" type="text/javascript">
//<![CDATA[

<!-- Begin
    document.oncontextmenu = function(){return false}
    // End -->
    //]]>
    </script>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand">Productos</a>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="../TC/index.php">Inicio</a></li>
            <li><a href="../TCF/index.htm">FAQ</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Productos</h1>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="../../img/p1.jpg" class="img-responsive" alt="Generic placeholder thumbnail" style="width: 140px; height: 140px;">
              <h4>Disco Duro de 1 Petabyte</h4>
              <span class="text-muted">Precio $1'000.000</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="../../img/p2.jpg" class="img-responsive" alt="Generic placeholder thumbnail" style="width: 140px; height: 140px;">
              <h4>Lunas Personalizadas</h4>
              <span class="text-muted">Precio $2'000.000</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="../../img/p3.png" class="img-responsive" alt="Generic placeholder thumbnail" style="width: 140px; height: 140px;">
              <h4>Dota 3</h4>
              <span class="text-muted">Precio $100.000</span>
            </div>
                        <div class="col-xs-6 col-sm-3 placeholder">
              <img src="../../img/p4.jpg" class="img-responsive" alt="Generic placeholder thumbnail" style="width: 140px; height: 140px;">
              <h4>Gintel Lentium 4</h4>
              <span class="text-muted">Precio $4'000.000</span>
            </div>
          </div>

                  <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="../../img/p5.jpg" class="img-responsive" alt="Generic placeholder thumbnail" style="width: 140px; height: 140px;">
              <h4>Tarjeta En-Vidia de 100 Terabytes</h4>
              <span class="text-muted">Precio $1'500.000</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="../../img/p6.jpg" class="img-responsive" alt="Generic placeholder thumbnail" style="width: 140px; height: 140px;">
              <h4>Mortal Kombat Edición Linux vs Windows</h4>
              <span class="text-muted">Precio $150.000</span>
            </div>
          </div>

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../../js/bootstrap.min.js"></script>
    <script src="../../js/docs.min.js"></script>
</body>
</html>
